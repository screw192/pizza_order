import axiosOrders from "../../axios_orders";
import {fetchDishesList} from "./dishesActions";

export const FETCH_ORDERS_REQUEST = "FETCH_ORDERS_REQUEST";
export const FETCH_ORDERS_SUCCESS = "FETCH_ORDERS_SUCCESS";
export const FETCH_ORDERS_FAILURE = "FETCH_ORDERS_FAILURE";

export const COMPLETE_ORDER_REQUEST = "COMPLETE_ORDER_REQUEST";
export const COMPLETE_ORDER_SUCCESS = "COMPLETE_ORDER_SUCCESS";
export const COMPLETE_ORDER_FAILURE = "COMPLETE_ORDER_FAILURE";

export const fetchOrdersRequest = () => ({type: FETCH_ORDERS_REQUEST});
export const fetchOrdersSuccess = orders => ({type: FETCH_ORDERS_SUCCESS, orders});
export const fetchOrdersFailure = () => ({type: FETCH_ORDERS_FAILURE});

export const fetchOrders = () => {
  return async dispatch => {
    try {
      dispatch(fetchOrdersRequest());
      await dispatch(fetchDishesList()); //нужен ли AWAIT? И нужно/можно ли вынести запрос за пределы try/catch?

      const response = await axiosOrders.get("/orders.json");
      dispatch(fetchOrdersSuccess(response.data));
    } catch (error) {
      dispatch(fetchOrdersFailure());
    }
  };
};

export const completeOrderRequest = () => ({type: COMPLETE_ORDER_REQUEST});
export const completeOrderSuccess = orders => ({type: COMPLETE_ORDER_SUCCESS, orders});
export const completeOrderFailure = () => ({type: COMPLETE_ORDER_FAILURE});

export const completeOrder = id => {
  return async dispatch => {
    try {
      dispatch(completeOrderRequest());

      await axiosOrders.delete(`/orders/${id}.json`);
      dispatch(completeOrderSuccess());
      dispatch(fetchOrders());
    } catch (error) {
      dispatch(completeOrderFailure());
    }
  };
};