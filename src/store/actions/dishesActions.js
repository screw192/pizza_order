import axiosOrders from "../../axios_orders";

export const FETCH_DISHES_LIST_REQUEST = "FETCH_DISHES_LIST_REQUEST";
export const FETCH_DISHES_LIST_SUCCESS = "FETCH_DISHES_LIST_SUCCESS";
export const FETCH_DISHES_LIST_FAILURE = "FETCH_DISHES_LIST_FAILURE";

export const DELETE_DISH_REQUEST = "DELETE_DISH_REQUEST";
export const DELETE_DISH_SUCCESS = "DELETE_DISH_SUCCESS";
export const DELETE_DISH_FAILURE = "DELETE_DISH_FAILURE";

export const fetchDishesListRequest = () => ({type: FETCH_DISHES_LIST_REQUEST});
export const fetchDishesListSuccess = dishesData => ({type: FETCH_DISHES_LIST_SUCCESS, dishesData});
export const fetchDishesListFailure = () => ({type: FETCH_DISHES_LIST_FAILURE});

export const deleteDishRequest = () => ({type: DELETE_DISH_REQUEST});
export const deleteDishSuccess = () => ({type: DELETE_DISH_SUCCESS});
export const deleteDishFailure = () => ({type: DELETE_DISH_FAILURE});

export const fetchDishesList = () => {
  return async dispatch => {
    try {
      dispatch(fetchDishesListRequest());

      const response = await axiosOrders.get("/dishes.json");
      dispatch(fetchDishesListSuccess(response.data));
    } catch (error) {
      dispatch(fetchDishesListFailure(error));
    }
  };
};

export const deleteDish = id => {
  return async dispatch => {
    try {
      dispatch(deleteDishRequest());

      await axiosOrders.delete(`/dishes/${id}.json`);
      dispatch(deleteDishSuccess())
      dispatch(fetchDishesList());
    } catch (error) {
      dispatch(deleteDishFailure(error));
    }
  }
}