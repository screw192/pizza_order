import axiosOrders from "../../axios_orders";
import {fetchDishesList} from "./dishesActions";

export const CHANGE_DISH_INPUT = "CHANGE_DISH_INPUT";
export const FILL_DISH_INPUT = "FILL_DISH_INPUT";
export const INIT_DISH_INPUT = "INIT_DISH_INPUT";

export const POST_DISH_REQUEST = "POST_DISH_REQUEST";
export const POST_DISH_SUCCESS = "POST_DISH_SUCCESS";
export const POST_DISH_FAILURE = "POST_DISH_FAILURE";

export const PUT_DISH_REQUEST = "PUT_DISH_REQUEST";
export const PUT_DISH_SUCCESS = "PUT_DISH_SUCCESS";
export const PUT_DISH_FAILURE = "PUT_DISH_FAILURE";

export const changeDishInput = (name, value) => ({type: CHANGE_DISH_INPUT, name, value});
export const fillDishInput = dishData => ({type: FILL_DISH_INPUT, dishData});
export const initDishInput = () => ({type: INIT_DISH_INPUT});

export const postDishRequest = () => ({type: POST_DISH_REQUEST});
export const postDishSuccess = () => ({type: POST_DISH_SUCCESS});
export const postDishFailure = () => ({type: POST_DISH_FAILURE});

export const postDish = (event, newDishData) => {
  return async dispatch => {
    event.preventDefault();
    try {
      dispatch(postDishRequest());

      await axiosOrders.post("/dishes.json", newDishData);
      dispatch(postDishSuccess());
      dispatch(fetchDishesList());
    } catch (error) {
      dispatch(postDishFailure(error));
    }
  };
};

export const putDishRequest = () => ({type: PUT_DISH_REQUEST});
export const putDishSuccess = () => ({type: PUT_DISH_SUCCESS});
export const putDishFailure = () => ({type: PUT_DISH_FAILURE});

export const putDish = (event, newDishData, id) => {
  return async dispatch => {
    event.preventDefault();
    try {
      dispatch(putDishRequest());

      await axiosOrders.put(`/dishes/${id}.json`, newDishData);
      dispatch(putDishSuccess());
      dispatch(fetchDishesList());
    } catch (error) {
      dispatch(putDishFailure(error));
    }
  };
};