import {
  COMPLETE_ORDER_FAILURE,
  COMPLETE_ORDER_REQUEST,
  COMPLETE_ORDER_SUCCESS,
  FETCH_ORDERS_FAILURE,
  FETCH_ORDERS_REQUEST,
  FETCH_ORDERS_SUCCESS
} from "../actions/ordersActions";

const initialState = {
  orders: {},
  delivery: 150,
  loading: false,
};

const ordersReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ORDERS_REQUEST:
      return {...state, loading: true};
    case FETCH_ORDERS_SUCCESS:
      return {...state, orders: action.orders, loading: false};
    case FETCH_ORDERS_FAILURE:
      return {...state, loading: false}; // ADD ERROR TO STATE AND ARG HERE...
    case COMPLETE_ORDER_REQUEST:
      return {...state, loading: true};
    case COMPLETE_ORDER_SUCCESS:
      return {...state, loading: false};
    case COMPLETE_ORDER_FAILURE:
      return {...state, loading: false}; // ADD ERROR TO STATE AND ARG HERE...
    default:
      return state;
  }
};

export default ordersReducer;