import {
  DELETE_DISH_FAILURE,
  DELETE_DISH_REQUEST, DELETE_DISH_SUCCESS,
  FETCH_DISHES_LIST_FAILURE,
  FETCH_DISHES_LIST_REQUEST,
  FETCH_DISHES_LIST_SUCCESS
} from "../actions/dishesActions";

const initialState = {
  dishes: {},
  loading: false,
};

const dishReducer = (state= initialState, action) => {
  switch (action.type) {
    case FETCH_DISHES_LIST_REQUEST:
      return {...state, loading: true};
    case FETCH_DISHES_LIST_SUCCESS:
      return {...state, loading: false, dishes: action.dishesData};
    case FETCH_DISHES_LIST_FAILURE:
      return {...state, loading: false}; // ADD ERROR TO STATE AND ARG HERE...
    case DELETE_DISH_REQUEST:
      return {...state, loading: true};
    case DELETE_DISH_SUCCESS:
      return {...state, loading: false};
    case DELETE_DISH_FAILURE:
      return {...state, loading: false}; // ADD ERROR TO STATE AND ARG HERE...
    default:
      return state;
  }
};

export default dishReducer;