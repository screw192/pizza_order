import {
  CHANGE_DISH_INPUT,
  FILL_DISH_INPUT,
  INIT_DISH_INPUT,
  POST_DISH_FAILURE,
  POST_DISH_REQUEST,
  POST_DISH_SUCCESS,
  PUT_DISH_FAILURE,
  PUT_DISH_REQUEST,
  PUT_DISH_SUCCESS
} from "../actions/addEditDishActions";

const initialState = {
  title: "",
  price: "",
  image: "",
  loading: false
};

const dishReducer = (state= initialState, action) => {
  switch (action.type) {
    case INIT_DISH_INPUT:
      return {...initialState};
    case CHANGE_DISH_INPUT:
      return {...state, [action.name]: action.value};
    case FILL_DISH_INPUT:
      return {...action.dishData};
    case PUT_DISH_REQUEST:
      return {...state, loading: true};
    case PUT_DISH_SUCCESS:
      return {...state, loading: false};
    case PUT_DISH_FAILURE:
      return {...state, loading: false};
    case POST_DISH_REQUEST:
      return {...state, loading: true};
    case POST_DISH_SUCCESS:
      return {...state, loading: false};
    case POST_DISH_FAILURE:
      return {...state, loading: false};
    default:
      return state;
  }
};

export default dishReducer;