import axios from "axios";

const axiosOrders = axios.create({
  baseURL: "https://screw192-js9-pizza-order-default-rtdb.firebaseio.com/"
});

export default axiosOrders;