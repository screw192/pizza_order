import React from "react";
import {Route, Switch} from "react-router-dom";

import Layout from "./components/UI/Layout/Layout";
import Dishes from "./components/Dishes/Dishes";
import AddEditDish from "./components/AddEditDish/AddEditDish";
import Orders from "./components/Orders/Orders";

const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={Dishes} />
        <Route path="/add" exact component={AddEditDish} />
        <Route path="/edit/:id" exact component={AddEditDish} />
        <Route path="/orders" exact component={Orders} />
      </Switch>
    </Layout>
);

export default App;