import React from 'react';

import Navigation from "../../Navigation/Navigation";
import {useSelector} from "react-redux";
import Preloader from "../Preloader/Preloader";

const Layout = props => {
  const isLoadingDishes = useSelector(state => state.dishes.loading);
  const isLoadingOrders = useSelector(state => state.orders.loading);
  const isLoadingAddEditDish = useSelector(state => state.addEditDish.loading);

  let preloader = null;

  if (isLoadingDishes || isLoadingOrders || isLoadingAddEditDish) {
    preloader = <Preloader/>;
  }

  return (
      <>
        {preloader}
        <Navigation/>
        <main className="Layout-Content">
          {props.children}
        </main>
      </>
  );
};

export default Layout;