import React from 'react';

import "./Button.css";

const Button = ({label, additionalClass, isDisabled = false, handler}) => {
  const buttonClasses = ["Button", "Button_" + additionalClass];
  return (
      <button
          className={buttonClasses.join(" ")}
          disabled={isDisabled}
          onClick={handler}
      >
        {label}
      </button>
  );
};

export default Button;