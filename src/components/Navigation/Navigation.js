import React from 'react';
import {NavLink} from "react-router-dom";

import "./Navigation.css";

const Navigation = () => {
  return (
      <header className="Header">
        <div className="container HeaderInner">
          <h2 className="HeaderTitle">Pizza delivery admin</h2>
          <nav className="HeaderNavigation">
            <NavLink
                exact to="/"
                className="HeaderNavigationItem"
            >Dishes</NavLink>
            <NavLink
                exact to="/orders"
                className="HeaderNavigationItem"
            >Orders</NavLink>
          </nav>
        </div>
      </header>
  );
};

export default Navigation;