import React from 'react';
import {useDispatch} from "react-redux";

import {completeOrder} from "../../../store/actions/ordersActions";
import Button from "../../UI/Button/Button";
import "./Order.css";

const Order = ({id, orderDetails, sum}) => {
  const dispatch = useDispatch();

  const deleteOrderHandler = () => dispatch(completeOrder(id))

  return (
      <div className="Order">
        <div className="OrderDetails">
          {orderDetails}
        </div>
        <div className="OrderSummary">
          <p className="OrderSummaryLabel">Order total:</p>
          <p className="OrderSummaryPrice"><b>{sum}</b></p>
          <Button
              label="Complete order"
              additionalClass="completeOrder"
              handler={deleteOrderHandler}
          />
        </div>
      </div>
  );
};

export default Order;