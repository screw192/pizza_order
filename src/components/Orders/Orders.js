import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import {fetchOrders} from "../../store/actions/ordersActions";
import Order from "./Order/Order";
import "./Orders.css";

const Orders = () => {
  const dispatch = useDispatch();

  const orders = useSelector(state => state.orders.orders);
  const dishes = useSelector(state => state.dishes.dishes);
  const delivery = useSelector(state => state.orders.delivery);

  const ordersList = Object.keys(orders).map(orderKey => {
    let orderItemsSumCounter = delivery;

    const orderDetails = Object.keys(orders[orderKey]).map(orderItem => {
      const itemName = dishes[orderItem].title;
      const itemQty = orders[orderKey][orderItem];
      const itemSum = dishes[orderItem].price * orders[orderKey][orderItem];
      orderItemsSumCounter += itemSum;

      return (
          <div
              className="ItemInfo"
              key={orderKey + orderItem}
          >
            <p className="ItemQtyAndName">{itemQty} x {itemName}</p>
            <p className="ItemTotal"><b>{itemSum} KGS</b></p>
          </div>
      );
    });

    orderDetails.push(
        <div
            className="ItemInfo"
            key={orderKey + "delivery"}
        >
          <p className="ItemQtyAndName">Delivery</p>
          <p className="ItemTotal"><b>{delivery} KGS</b></p>
        </div>
    );

    return (
        <Order
            key={orderKey}
            id={orderKey}
            orderDetails={orderDetails}
            sum={orderItemsSumCounter}
        />
    );
  });

  useEffect(() => {
    dispatch(fetchOrders());
  }, [dispatch]);

  return (
      <div className="container">
        <div className="Orders">
          <h4 className="OrdersTitle">Orders</h4>
          {ordersList}
        </div>
      </div>
  );
};

export default Orders;