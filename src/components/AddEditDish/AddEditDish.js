import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {changeDishInput, fillDishInput, initDishInput, postDish, putDish} from "../../store/actions/addEditDishActions";
import Button from "../UI/Button/Button";

import "./AddEditDish.css";

const AddEditDish = props => {
  const dispatch = useDispatch();
  const dishData = useSelector(state => state.addEditDish);
  const existingDishData = useSelector(state => state.dishes.dishes[props.match.params.id]);

  React.useEffect(() => {
    if (props.match.params.id) {
      dispatch(fillDishInput(existingDishData));
    } else {
      dispatch(initDishInput());
    }
  }, [dispatch, props.match.params.id, existingDishData]);

  let formTitle;
  let saveDishHandler;
  if (props.match.params.id) {
    formTitle = "Edit dish";
    saveDishHandler = event => {
      dispatch(putDish(event, dishData, props.match.params.id));
      props.history.push("/");
    };
  } else {
    formTitle = "Add new dish";
    saveDishHandler = event => {
      dispatch(postDish(event, dishData));
      props.history.push("/");
    };
  }

  const onChangeHandler = event => dispatch(changeDishInput(event.target.name, event.target.value));

  return (
      <div className="container">
        <h4 className="AddEditFormTitle">{formTitle}</h4>
        <div className="AddEditForm">
          <form>
            <div className="formRow">
              <label htmlFor="dishName">Name:</label>
              <input
                  id="dishName"
                  name="title"
                  type="text"
                  value={dishData.title}
                  onChange={onChangeHandler}
              />
            </div>
            <div className="formRow">
              <label htmlFor="dishPrice">Price:</label>
              <input
                  id="dishPrice"
                  name="price"
                  type="text"
                  value={dishData.price}
                  onChange={onChangeHandler}
              />
            </div>
            <div className="formRow">
              <label htmlFor="dishImage">Image link:</label>
              <input
                  id="dishImage"
                  name="image"
                  type="text"
                  value={dishData.image}
                  onChange={onChangeHandler}
              />
            </div>
            <Button
                label="Save"
                additionalClass="saveDish"
                handler={saveDishHandler}
            />
          </form>
        </div>
      </div>
  );
};

export default AddEditDish;