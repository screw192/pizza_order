import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";

import {deleteDish, fetchDishesList} from "../../store/actions/dishesActions";
import Dish from "./Dish/Dish";
import "./Dishes.css";

const Dishes = () => {
  const dishes = useSelector(state => state.dishes.dishes);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchDishesList());
  }, [dispatch]);

  const dishesList = Object.keys(dishes).map(key => {
    const deleteHandler = () => dispatch(deleteDish(key));

    return (
        <Dish
            key={key}
            id={key}
            image={dishes[key].image}
            name={dishes[key].title}
            price={dishes[key].price}
            deleteHandler={deleteHandler}
        />
    );
  });

  return (
      <div className="container">
        <div className="Dishes">
          <div className="DishesTop">
            <h4 className="DishesTitle">Dishes</h4>
            <Link
                to={"/add"}
                className="AddDishButton"
            >Add new dish</Link>
          </div>
          {dishesList}
        </div>
      </div>
  );
};

export default Dishes;