import React from 'react';
import {Link} from "react-router-dom";

import Button from "../../UI/Button/Button";
import "./Dish.css";

const Dish = ({id, image, name, price, deleteHandler}) => {
  return (
      <div className="Dish">
        <img src={image} alt="" className="DishImage" width="100px" height="auto"/>
        <p className="DishName">{name}</p>
        <p className="DishPrice"><b>{price} KGS</b></p>
        <Link
            to={"/edit/" + id}
            className="DishEdit"
        >
          Edit
        </Link>
        <Button
            label="Delete"
            additionalClass="dishDelete"
            handler={deleteHandler}
        />
      </div>
  );
};

export default Dish;